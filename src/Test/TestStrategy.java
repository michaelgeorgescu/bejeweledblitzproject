package Test;

import Main.Board;
import Main.BoardLoader;
import Main.Strategy;
import org.junit.Test;

/**
 * Created by michael on 5/26/15.
 */
public class TestStrategy {

    @Test
    public void testPerformGreedyStrategy() {
        try {
            Thread.sleep(4000);
        } catch (Exception e) {

        }
        Board board = new Board();
        BoardLoader boardLoader = new BoardLoader();
        Strategy strategy = new Strategy(board, boardLoader);
        strategy.performGreedyStrategy();
    }
}
