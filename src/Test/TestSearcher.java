package Test;

import Main.Board;
import Main.Move;
import Main.Searcher;
import org.junit.Assert;
import org.junit.Test;

import java.util.PriorityQueue;

/**
 * Created by michael on 5/24/15.
 */
public class TestSearcher {

    @Test
    public void testCheckFor3MatchCase1() {
        Board testBoard = new Board();
        //*
        //
        //*
        //*
        int[] boardColorTypes =
               {2,7,2,4,2,2,6,5,
                5,6,3,1,1,3,5,4,
                5,2,5,7,4,3,4,7,
                1,7,3,2,5,2,1,2,
                6,2,6,2,6,7,5,7,
                4,3,5,5,3,3,1,5,
                3,1,6,6,7,4,1,3,
                7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase1(3, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase2() {
        Board testBoard = new Board();
        //* **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,5,7,
                        4,3,5,5,3,3,1,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase2(1, 2, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase3() {
        Board testBoard = new Board();
        //*
        //*
        //
        //* <-
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,1,7,
                        4,3,5,5,3,3,5,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase3(6, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase4() {
        Board testBoard = new Board();
        //   * <-
        // **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,5,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,1,7,
                        4,3,5,5,3,3,5,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase4(0, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase5() {
        Board testBoard = new Board();
        // ** * <-
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,1,7,
                        4,3,5,5,3,3,5,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase5(0, 7, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase6() {
        Board testBoard = new Board();
        // **
        //   *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,2,7,1,7,
                        4,3,5,5,3,2,5,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase6(5, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase7() {
        Board testBoard = new Board();
        // **
        //*
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,2,3,
                        6,2,6,2,2,7,1,7,
                        4,3,5,5,3,2,5,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase7(4, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase8() {
        Board testBoard = new Board();
        //*
        // **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,2,3,
                        6,2,6,2,4,7,1,7,
                        4,3,5,5,3,4,4,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase8(4, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase9() {
        Board testBoard = new Board();
        // *<-
        //* *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,2,3,
                        6,2,6,2,5,7,1,7,
                        4,3,5,5,3,5,4,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase9(4, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase10() {
        Board testBoard = new Board();
        //* *
        // *<-
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,5,2,5,2,3,
                        6,2,6,2,5,7,1,7,
                        4,3,5,5,3,5,4,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase10(4, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor4MatchCase1() {
        Board testBoard = new Board();
        // *
        //* **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,2,5,7,1,7,
                        4,3,5,5,3,5,4,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase1(2, 2, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor4MatchCase2() {
        Board testBoard = new Board();
        // *
        //* **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,2,5,7,1,7,
                        4,3,5,5,3,5,4,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase2(4, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor4MatchCase3() {
        Board testBoard = new Board();
        //* **
        // * <-
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,4,7,4,4,
                        4,3,5,5,3,4,3,5,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase3(5, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor4MatchCase4() {
        Board testBoard = new Board();
        //** *
        //  * <-
        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,4,7,4,3,
                        4,3,5,5,3,4,3,5,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase4(5, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase1() {
        Board testBoard = new Board();

        int[] boardColorTypes =
                {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,5,7,4,3,
                        4,3,5,5,3,5,3,5,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase1(5, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase2() {
        Board testBoard = new Board();

        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,5,4,4,3,
                        4,3,5,5,4,5,4,4,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,4,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase2(5, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase3() {
        Board testBoard = new Board();

        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,5,4,4,3,
                        4,3,5,5,2,5,4,4,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,4,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase3(4, 5, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor5MatchCase4() {
        Board testBoard = new Board();

        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,5,4,4,3,
                        4,3,5,5,2,5,5,4,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,5,4,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase4(4, 4, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor5MatchCase5() {
        Board testBoard = new Board();

        int[] boardColorTypes =
                       {2,7,2,4,2,2,3,2,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,5,3,5,5,5,2,3,
                        6,2,6,4,3,4,4,3,
                        4,3,5,5,2,5,5,4,
                        3,1,6,6,5,4,1,3,
                        7,1,4,2,3,4,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase5(6, 4, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor5MatchCase6() {
        Board testBoard = new Board();
        // ** *
        //   *
        //   *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,5,7,
                        4,5,5,2,5,3,1,5,
                        3,1,6,5,7,4,1,3,
                        7,1,4,5,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase6(5, 4, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor5MatchCase7() {
        Board testBoard = new Board();
        //   *
        // **
        //   *
        //   *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,6,7,5,7,
                        4,5,5,2,4,3,1,5,
                        3,1,6,5,7,4,1,3,
                        7,1,4,5,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase7(4, 3, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor5MatchCase8() {
        Board testBoard = new Board();
        // *
        //* **
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,6,7,5,7,
                        4,2,5,2,5,5,1,5,
                        3,1,6,5,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase8(5, 2, testBoard);
        Assert.assertEquals(true, result);

    }
    @Test
    public void testCheckFor4MatchCase5() {
        Board testBoard = new Board();
        // *
        //  *
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,6,7,2,7,
                        4,2,5,2,5,2,1,5,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase5(4, 6, testBoard);
        Assert.assertEquals(true, result);

    }
    @Test
    public void testCheckFor4MatchCase6() {
        Board testBoard = new Board();
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,2,7,5,7,
                        4,2,5,2,5,2,1,5,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase6(4, 4, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor4MatchCase7() {
        Board testBoard = new Board();
        // *
        // *
        //  *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,2,5,1,2,5,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase7(5, 6, testBoard);
        Assert.assertEquals(true, result);

    }

    @Test
    public void testCheckFor4MatchCase8() {
        Board testBoard = new Board();
        // *
        // *
        //  *
        // *
        int[] boardColorTypes =
                {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,2,2,1,1,5,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFourMatchCase8(5, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase9() {
        Board testBoard = new Board();
        // *
        // *
        //  **
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,5,1,2,2,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase9(6, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase10() {
        Board testBoard = new Board();
        // *
        // *
        //*  **
        //
        int[] boardColorTypes =
                {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,2,2,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase10(5, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase11() {
        Board testBoard = new Board();
        // *
        // *
        //* *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,2,1,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase11(6, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase12() {
        Board testBoard = new Board();
        // *
        //* *
        // *
        // *
        int[] boardColorTypes =
                {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,1,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,2,1,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase12(4, 5, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase13() {
        Board testBoard = new Board();
        // *
        // *
        //  *
        // *
        // *
        int[] boardColorTypes =
                {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,2,1,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase13(5, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor5MatchCase14() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,1,1,
                        3,1,6,5,7,2,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForFiveMatchCase14(5, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase11() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,2,1,1,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase11(5, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase12() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,6,1,2,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase12(5, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase13() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                        {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,1,2,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,6,2,7,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase13(3, 6, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase14() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,1,7,
                        1,7,3,2,2,1,4,2,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,6,2,7,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase14(3, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase15() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,2,1,7,
                        1,7,3,5,2,1,4,1,
                        6,2,6,5,3,2,5,7,
                        4,2,5,3,6,3,7,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase15(3, 4, testBoard);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testCheckFor3MatchCase16() {
        Board testBoard = new Board();
        // *
        // *
        //*
        // *
        // *
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,2,4,1,1,7,
                        1,7,3,5,2,1,4,1,
                        6,2,6,2,3,2,5,7,
                        4,2,5,3,6,3,7,1,
                        3,1,6,5,7,1,1,3,
                        7,1,4,2,5,2,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        boolean result = searcher.checkForThreeMatchCase16(3, 4, testBoard);
        Assert.assertEquals(true, result);
    }
/*
    @Test
    public void testCheckForMatches() {
        Board testBoard = new Board();
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,5,7,
                        4,5,5,2,5,3,1,5,
                        3,1,6,5,7,4,1,3,
                        7,1,4,5,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        searcher.findMatches(testBoard);
        PriorityQueue<Move> moves = searcher.getPriorityQueue();
        for(Move move : moves) {
            System.out.println(move);
        }
    }
*/
    @Test
    public void testLowerRowMatchGetsPriority() {
        Board testBoard = new Board();
        int[] boardColorTypes =
                       {1,7,2,4,2,2,6,5,
                        5,6,3,1,1,3,5,4,
                        5,2,5,7,4,3,2,7,
                        1,7,3,2,5,2,5,2,
                        6,2,6,2,6,7,5,7,
                        4,5,1,2,5,1,2,5,
                        3,1,6,5,7,3,1,3,
                        7,1,4,5,3,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            testBoard.add(boardColorTypes[i]);
        }
        Searcher searcher = new Searcher();
        searcher.findMatches(testBoard);
        PriorityQueue<Move> moves = searcher.getPriorityQueue();
        for(Move move : moves) {
            System.out.println(move);
        }
    }



}
