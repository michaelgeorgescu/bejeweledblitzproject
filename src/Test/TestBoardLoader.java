package Test;

import Main.Board;
import Main.BoardLoader;
import org.junit.Test;

import java.awt.*;

/**
 * Created by michael on 5/24/15.
 */
public class TestBoardLoader {

    @Test
    public void testLoad() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Board board = new Board();
        BoardLoader boardLoader = new BoardLoader();
        boardLoader.load(board);
        System.out.println(board.toString());
    }

    @Test
    public void testGetColorAtPosition() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            Robot robot = new Robot();
            Color color = robot.getPixelColor(325, 416);
            System.out.println(color);
        } catch(Exception e) {

        }


    }
}
