package Test;

import org.junit.Assert;
import org.junit.Test;
import Main.Board;

import java.util.ArrayList;

/**
 * Created by michael on 5/24/15.
 */
public class TestBoard extends Board{


    @Test(timeout = 10000)
    public void testPositionInitialized() {
        Board board = new Board();
        ArrayList<Integer> startPosition = board.getPosition();
        ArrayList<Integer> startPositionExpected = new ArrayList<Integer>();
        startPositionExpected.add(0);
        startPositionExpected.add(0);
        Assert.assertEquals(startPositionExpected.get(0), startPosition.get(0));
        Assert.assertEquals(startPositionExpected.get(1), startPosition.get(1));
    }

    @Test
    public void testIncrementPosition() {
        Board board = new Board();
        board.incrementPosition();
        ArrayList<Integer> positionExpected = new ArrayList<Integer>();
        positionExpected.add(0);
        positionExpected.add(1);
        ArrayList<Integer> position = board.getPosition();
        System.out.println(position);
        Assert.assertEquals(positionExpected.get(0), position.get(0));
        Assert.assertEquals(positionExpected.get(1), position.get(1));
    }

    @Test
    public void testTenIncrements() {
        Board board = new Board();
        for(int i = 0; i < 10; i++) {
            board.incrementPosition();
            System.out.println(board.getPosition());
        }
    }

    @Test
    public void testResetPosition() {
        Board board = new Board();
        board.resetPosition();
        ArrayList<Integer> position = board.getPosition();
        ArrayList<Integer> positionExpected = new ArrayList<Integer>();
        positionExpected.add(0);
        positionExpected.add(0);
        Assert.assertEquals(positionExpected.get(0), position.get(0));
        Assert.assertEquals(positionExpected.get(1), position.get(1));

    }

    @Test
    public void testGetGemAtPosition() {
        Board testBoard = new Board();
        //* **
        int[] boardColorTypes =
                       {2,7,2,4,2,2,6,5,
                        5,6,3,1,3,3,5,4,
                        5,2,5,7,4,3,4,7,
                        1,7,3,2,5,2,1,2,
                        6,2,6,2,6,7,5,7,
                        4,3,5,5,3,3,1,5,
                        3,1,6,6,7,4,1,3,
                        7,1,4,2,5,7,3,2};
        for(int i = 0; i < boardColorTypes.length; i++) {
            System.out.println(boardColorTypes[i]);
            testBoard.add(boardColorTypes[i]);
        }
        System.out.println(testBoard.toString());
        int gemType = testBoard.getGemAtPosition(0,0);
        Assert.assertEquals(2, gemType);
    }
}
