package Test;

import Main.Board;
import Main.GemColorIdentifier;
import junit.framework.Assert;
import org.junit.Test;

import java.awt.*;

/**
 * Created by michael on 6/3/15.
 */
public class TestGemColorIdentifier {

    @Test
    public void testIdentifyColorPurple() {
        Color color = new Color(230, 0, 231);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_PURPLE, gemType);
    }

    @Test
    public void testIdentifyColorYellow() {
        Color color = new Color(255, 248, 0);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_YELLOW, gemType);
    }

    @Test
    public void testIdentifyColorWhite() {
        Color color = new Color(255, 248, 250);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_WHITE, gemType);
    }

    @Test
    public void testIdentifyColorBlue() {
        Color color = new Color(0, 109, 255);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_BLUE, gemType);
    }

    @Test
    public void testIdentifyColorGreen() {
        Color color = new Color(14, 217, 0);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_GREEN, gemType);
    }

    @Test
    public void testIdentifyColorOrange() {
        Color color = new Color(254, 94, 0);

        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_ORANGE, gemType);
    }

    @Test
    public void testIdentifyColorRed() {
        Color color = new Color(240, 0, 36);
        int gemType = GemColorIdentifier.identifyColor(color);
        Assert.assertEquals(Board.TYPE_RED, gemType);
    }

}
