package Test;

import Main.Move;
import Main.Player;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 5/24/15.
 */
public class TestPlayer {
/*
    @Test
    public void testDragRight() {
        try {
            Thread.sleep(2000);
            Robot robot = new Robot();
            Player player = new Player(robot);
            player.dragRight(203, 417);
            //player.dragRight(35, 35);
        } catch (Exception e) {

        }
    }

    @Test
    public void testDragLeft() {
        //243, 418
        try {
            Thread.sleep(2000);
            Robot robot = new Robot();
            Player player = new Player(robot);
            player.dragLeft(243, 417);
            //player.dragRight(35, 35);
        } catch (Exception e) {

        }
    }

    @Test
    public void testDragDown() {
        //243, 418
        try {
            Thread.sleep(2000);
            Robot robot = new Robot();
            Player player = new Player(robot);
            player.dragDown(203, 417);
            //player.dragRight(35, 35);
        } catch (Exception e) {

        }
    }

    @Test
    public void testDragUp() {
        //243, 418
        try {
            Thread.sleep(2000);
            Robot robot = new Robot();
            Player player = new Player(robot);
            player.dragUp(203, 457);
        } catch (Exception e) {

        }
    }
*/
    @Test
    public void testExecuteMove() {
        Robot robot = null;
        try {
            Thread.sleep(4000);
            robot = new Robot();

        } catch(Exception e) {

        }
        List<Integer> position = new ArrayList<Integer>();
        position.add(7);
        position.add(7);
        Move move = new Move(1, position, Move.MOVE_3_MATCH); // drag down
        Player player = new Player(robot);
        player.executeMove(move);
    }

    @Test
    public void testExecuteMoveDragDownAt65() {

        Robot robot = null;
        try {
            Thread.sleep(4000);
            robot = new Robot();

        } catch(Exception e) {

        }
        List<Integer> position = new ArrayList<Integer>();
        position.add(5);
        position.add(6);
        Move move = new Move(2, position, 4); // drag down
        Player player = new Player(robot);
        player.executeMove(move);
    }

}
