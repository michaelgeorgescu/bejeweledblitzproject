package Main;

/**
 * Created by michael on 6/3/15.
 */
public class Configuration {

    public static final int TIME_DELAY = 40;
    public static final int NUMBER_OF_MOVES = 140;
}
