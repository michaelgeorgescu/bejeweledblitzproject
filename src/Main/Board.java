package Main;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by michael on 5/24/15.
 */
public class Board {
// stores the state of the board
    protected BoardLoader boardLoader;
    ArrayList<Integer> position;
    Color[][] board = new Color[8][8];
    int[][] boardColorTypes = new int[8][8];
    public static int TYPE_RED = 1;
    public static int TYPE_GREEN = 2;
    public static int TYPE_BLUE = 3;
    public static int TYPE_PURPLE = 4;
    public static int TYPE_ORANGE = 5;
    public static int TYPE_YELLOW = 6;
    public static int TYPE_WHITE = 7;
    public Board() {
        boardLoader = new BoardLoader();
        position = new ArrayList<Integer>();
        position.add(0);
        position.add(0);
        //boardLoader.load(this);
    }

    public ArrayList<Integer> getPosition() {
        return position;
    }

    public boolean incrementPosition() {
        if(!(position.get(0) == 7 && position.get(1) == 7)) {
            if (position.get(1).equals(7)) { //increment j only if you're at the end of the row
                position.set(0, position.get(0) + 1);
            }
            position.set(1, (position.get(1) + 1) % 8);
            return true;
        }
        return false;
    }

    public boolean containsZeroes() {
        for(int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if(boardColorTypes[i][j] == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public void add(Color color) {
        board[position.get(0)][position.get(1)] = color;
        incrementPosition();
    }

    public int getGemAtPosition(int i, int j) {
        int value = -1;
        try {
            value = boardColorTypes[i][j];
        } catch( Exception e) {

        }
        return value;
    }

    public void add(int gemType) {
        boardColorTypes[position.get(0)][position.get(1)] = gemType;
        incrementPosition();

    }

    public void setPosition(int i , int j) {
        position.set(0, i);
        position.set(1, j);
    }

    public void resetPosition() {
        setPosition(0, 0);
    }

    @Override
    public String toString() {
        String s = "";

        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board.length; j++) {
                //Color color = board[i][j];
                //s += color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + "     ";
                s += boardColorTypes[i][j];
            }
            s += "\n";
        }
        s += "\n\n";
/*
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board.length; j++) {
                Color color = board[i][j];
                s += color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + "     ";
            }
            s += "\n";
        }
        */

        return s;
    }
}
