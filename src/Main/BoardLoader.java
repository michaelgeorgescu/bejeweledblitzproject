package Main;

import Main.Board;

import java.awt.*;

/**
 * Created by michael on 5/24/15.
 */
public class BoardLoader {
    Robot robot;
    public static int START_ROW = 195;
    public static int START_COLUMN = 431;
    public static int INCREMENT = 40;
    //Reads the screen to load the board's state
    public BoardLoader() {
        try {
            robot = new Robot();
        } catch (Exception e) {

        }
    }

    public void load(Board board) {
        for(int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Color color = robot.getPixelColor(j * INCREMENT + START_ROW, i * INCREMENT + START_COLUMN);
                //board.add(color);
                    board.add(GemColorIdentifier.identifyColor(color));
            }
        }
    }

    public int getTypeFromColor(Color color) {
        if(color.getRed() == 254 && color.getGreen() == 202 && color.getBlue() == 0) {
            return Board.TYPE_YELLOW;
        }if(color.getRed() == 254 && color.getGreen() == 254 && color.getBlue() == 254) {
            return Board.TYPE_WHITE;
        }if(color.getRed() == 65 && color.getGreen() == 255 && color.getBlue() == 98) {
            return Board.TYPE_GREEN;
        }if(color.getRed() == 114 && color.getGreen() == 0 && color.getBlue() == 116) {
            return Board.TYPE_PURPLE;
        }if(color.getRed() == 16 && color.getGreen() == 109 && color.getBlue() == 255) {
            return Board.TYPE_BLUE;
        }if(color.getRed() == 254 && color.getGreen() == 232 && color.getBlue() == 92) {
            return Board.TYPE_ORANGE;
        }if(color.getRed() == 254 && color.getGreen() == 0 && color.getBlue() == 52) {
            return Board.TYPE_RED;
        }
        return 0;
    }


}
