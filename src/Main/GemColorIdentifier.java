package Main;

import java.awt.Color;

/**
 * Created by michael on 5/24/15.
 */
public class GemColorIdentifier {

    public static int identifyColor(Color color) {
        int red = color.getRed();
        int green = color.getGreen();
        int blue = color.getBlue();

        float[] hsb = Color.RGBtoHSB(red, green, blue, null);
        float hue = hsb[0];
        float saturation = hsb[1];
        System.out.println("saturation " + saturation);
        System.out.println(hue);
        //purple
        if(hue > .82 && hue < .84 ) {
            return Board.TYPE_PURPLE;
        }

        //yellow
        if(hue >= .11 && hue <= .2) {
            return Board.TYPE_YELLOW;
        }

        //white
        if(saturation >= 0 && saturation < 0.1) {
            return Board.TYPE_WHITE;
        }

        //blue
        if(hue > .5 && hue < .65) {
            return Board.TYPE_BLUE;
        }

        //green
        if(hue >= .3 && hue <= .43) {
            return Board.TYPE_GREEN;
        }

        //orange
        if(hue > .02 && hue < .07) {
            return Board.TYPE_ORANGE;
        }

        //red
        if(hue >= .95 && hue <= 1) {
            return Board.TYPE_RED;
        }

        return 0;
    }

    public static boolean isInRangeOf(double number, double low, double high) {
        if(number >= low && number <= high) {
            return true;
        }
        return false;
    }
}
