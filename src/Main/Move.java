package Main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michael on 5/25/15.
 */
public class Move implements Comparable {
    public static final int MOVE_5_MATCH_ROW = 1;
    public static final int MOVE_5_MATCH_T = 2;
    public static final int MOVE_5_MATCH_L = 3;
    public static final int MOVE_4_MATCH = 4;
    public static final int MOVE_3_MATCH = 5;

    //describes position and action
    private int action; // 1 of four types: drag_up, drag_down,...
    private List<Integer> position;
    private int moveType;

    public Move(int action, List<Integer> position, int moveType) {
        this.action = action;
        this.position = position;
        this.moveType = moveType;

    }

    @Override
    public int compareTo(Object o) {
        //for ordering move in a priority queue
        // greater moveType is a higher match
        if(((Move)o).getMoveType() < this.moveType) {
            return 1;
        }
        if(((Move)o).getMoveType() == this.moveType) {
            int colPosition = ((Move)o).getPosition().get(1);
            int colPositionThis = this.getPosition().get(1);
            if(colPosition > colPositionThis) {
                return 1; //lower position on board gets higher priority
            } else if(colPosition == colPositionThis) {
                return 0;
            } else if(colPosition < colPositionThis) {
                return -1;
            }
        }
        if(((Move)o).getMoveType() > this.moveType) {
            return -1;
        }
        return -1;
    }

    public int getAction() {
        return action;
    }

    public List<Integer> getPosition() {

        return position;
    }

    public List<Integer> getPositionOnScreen() {
        List<Integer> positionScreen = new ArrayList<Integer>();
        int position1 = BoardLoader.START_ROW + BoardLoader.INCREMENT * position.get(0);
        int position2 = BoardLoader.START_COLUMN + BoardLoader.INCREMENT * position.get(1);
        positionScreen.add(position1);
        positionScreen.add(position2);
        return positionScreen;
    }

    public int getMoveType() {
        return moveType;
    }

    @Override
    public String toString() {
        String s = "";
        s += "action: " + action + "\n"
          + "position: " + "(" + position.get(0) + ", " + position.get(1) + ")" + "\n"
          + "MoveType: " + moveType + "\n";
        return s;
    }
}
