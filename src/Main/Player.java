package Main;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.List;

/**
 * Created by michael on 5/24/15.
 */
public class Player {
    public static final int ACTION_TYPE_DRAG_UP = 1;
    public static final int ACTION_TYPE_DRAG_DOWN = 2;
    public static final int ACTION_TYPE_DRAG_RIGHT = 3;
    public static final int ACTION_TYPE_DRAG_LEFT = 4;
    public static final int INCREMENT = 40;
    Robot robot;
    //performs moves on the boards

    public Player(Robot robot) {
        this.robot = robot;
    }

    public void dragRight(int i, int j) {
        robot.mouseMove(i, j);
        for(int k = 0; k < INCREMENT; k++) {
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseMove(i + k, j);
        }

        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void executeMove(Move move) {
        List<Integer> position = move.getPositionOnScreen();
        switch(move.getAction()) {
            case ACTION_TYPE_DRAG_UP:
                dragUp(position.get(0), position.get(1));
                break;
            case ACTION_TYPE_DRAG_LEFT:
                dragLeft(position.get(0), position.get(1));
                break;
            case ACTION_TYPE_DRAG_DOWN:
                dragDown(position.get(0), position.get(1));
                break;
            case ACTION_TYPE_DRAG_RIGHT:
                dragRight(position.get(0), position.get(1));
                break;
        }
    }

    public void dragLeft(int i, int j) {
        robot.mouseMove(i, j);
        for(int k = 0; k < INCREMENT; k++) {
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseMove(i - k, j);
        }

        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void dragUp(int i, int j) {
        robot.mouseMove(i, j);
        for(int k = 0; k < INCREMENT; k++) {
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseMove(i, j - k);
        }

        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void dragDown(int i, int j) {
        robot.mouseMove(i, j);
        for(int k = 0; k < INCREMENT; k++) {
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseMove(i, j + k);
        }
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public static void main(String[] args) {


    }
}
