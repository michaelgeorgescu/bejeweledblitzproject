package Main;

import java.util.PriorityQueue;

/**
 * Created by michael on 5/26/15.
 */
public class Strategy {
    private Board board;
    private BoardLoader boardLoader;
    private Searcher searcher;

    public Strategy(Board board, BoardLoader boardLoader) {
        this.board = board;
        this.boardLoader = boardLoader;
        searcher = new Searcher();
    }



    public void performGreedyStrategy() {
        for(int i = 0; i < Configuration.NUMBER_OF_MOVES; i++) {
            board.resetPosition();// reset pointer before reloading
            try {
                Thread.sleep(Configuration.TIME_DELAY);
            } catch (Exception e) {

            }
            boardLoader.load(board);
            if(board.containsZeroes()) {
                continue; //board has not finished loading all new gems. skip to next iteration and wait for delay.

            }
            System.out.println(board);
            searcher.clearQueue();
            searcher.findMatches(board); // populates a priority queue
            PriorityQueue<Move> moves = searcher.getPriorityQueue();
            for(Move move :  moves) {
                System.out.println(move);
            }
            Player player = new Player(boardLoader.robot);

            if(!moves.isEmpty()) {
                player.executeMove(moves.poll());//execute highest priority move
            }
        }
        //performGreedyStrategy();
    }
}
