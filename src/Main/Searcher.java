package Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Created by michael on 5/24/15.
 */
public class Searcher {
    //searches the board to update the current state of the board
    private PriorityQueue<Move> priorityQueue;

    public Searcher() {

        priorityQueue = new PriorityQueue<Move>();
    }

    public void findMatches(Board board) {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                checkForMatches(i, j, board);
            }
        }
    }

    public PriorityQueue<Move> getPriorityQueue() {
        return priorityQueue;
    }

    public void clearQueue() {
        priorityQueue.clear();
    }

    public void checkForMatches(int i, int j, Board board) {
        if(checkForThreeMatchCase1(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase2(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase3(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase4(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase5(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase6(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase7(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase8(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase9(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase10(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase11(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase12(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase13(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase14(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase15(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForThreeMatchCase16(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_3_MATCH));
        }if(checkForFourMatchCase1(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase2(i, j, board)) {
        priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase3(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase4(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase5(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase6(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase7(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFourMatchCase8(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_4_MATCH));
        }if(checkForFiveMatchCase1(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_5_MATCH_T)); // missed the other T case
        }if(checkForFiveMatchCase2(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase3(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase4(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_5_MATCH_ROW)); // missed the other T case
        }if(checkForFiveMatchCase5(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_5_MATCH_ROW)); // missed the other T case
        }if(checkForFiveMatchCase6(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase7(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase8(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_5_MATCH_T)); // missed the other T case
        }if(checkForFiveMatchCase9(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase10(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_5_MATCH_L)); // missed the other T case
        }if(checkForFiveMatchCase11(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_UP, Arrays.asList(j, i), Move.MOVE_5_MATCH_T)); // missed the other T case
        }if(checkForFiveMatchCase12(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_DOWN, Arrays.asList(j, i), Move.MOVE_5_MATCH_T)); // missed the other T case
        }if(checkForFiveMatchCase13(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_LEFT, Arrays.asList(j, i), Move.MOVE_5_MATCH_ROW)); // missed the other T case
        }if(checkForFiveMatchCase14(i, j, board)) {
            priorityQueue.add(new Move(Player.ACTION_TYPE_DRAG_RIGHT, Arrays.asList(j, i), Move.MOVE_5_MATCH_ROW)); // missed the other T case
        }
    }

    public boolean checkForThreeMatchCase1(int i, int j, Board board) {
        //* <-
        //
        //*
        //*
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {
            if (board.getGemAtPosition(i + 2, j) == gemStartType && board.getGemAtPosition(i + 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase2(int i, int j, Board board) {
        //* **
        //|
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i, j + 2) == gemStartType && board.getGemAtPosition(i, j + 3) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase3(int i, int j, Board board) {
        //*
        //*
        //
        //* <-
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 2, j) == gemStartType && board.getGemAtPosition(i - 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase4(int i, int j, Board board) {
        //  *
        //**
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase5(int i, int j, Board board) {
        //** *<-
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {
            if (board.getGemAtPosition(i, j - 2) == gemStartType && board.getGemAtPosition(i, j - 3) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase6(int i, int j, Board board) {
        // **
        //   *<-

        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i - 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase7(int i, int j, Board board) {
        // **
        //* <-
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j + 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase8(int i, int j, Board board) {
        //* <-
        // **
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase9(int i, int j, Board board) {
        // * <-
        //* *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForThreeMatchCase10(int i, int j, Board board) {
        //* *
        // *<-
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j - 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase11(int i, int j, Board board) {
        // *
        // *
        //*
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i -2, j + 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase12(int i, int j, Board board) {
        // *
        // *
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i - 2, j - 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase13(int i, int j, Board board) {
        //  *
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 2, j - 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase14(int i, int j, Board board) {
        //*
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 2, j + 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase15(int i, int j, Board board) {
        // *
        //*
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 1) == gemStartType) {
            return true;
        }
        return false;
    }

    public boolean checkForThreeMatchCase16(int i, int j, Board board) {
        // *
        //  *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType) {
            return true;
        }
        return false;
    }



    public boolean checkForFourMatchCase1(int i,int j, Board board) {
        // * <-
        //* **
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j + 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase2(int i,int j, Board board) {
        //  * <-
        //** *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase3(int i,int j, Board board) {
        //* **
        // * <-
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j + 2) == gemStartType &&
                    board.getGemAtPosition(i - 1, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase4(int i,int j, Board board) {
        //** *
        //  * <-
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i - 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase5(int i,int j, Board board) {
        // *
        //  *
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 2, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase6(int i,int j, Board board) {
        // *
        //*
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 1) == gemStartType &&
                    board.getGemAtPosition(i + 2, j + 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase7(int i,int j, Board board) {
        // *
        // *
        //  *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i - 2, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFourMatchCase8(int i,int j, Board board) {
        // *
        // *
        //*
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 2, j + 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j + 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase1(int i,int j, Board board) {
        //   *
        // ** * <-
        //   *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i, j - 2) == gemStartType && board.getGemAtPosition(i, j - 3) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase2(int i,int j, Board board) {
        // * **
        //  *
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i, j + 2) == gemStartType && board.getGemAtPosition(i, j + 3) == gemStartType &&
                    board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 2, j + 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase3(int i,int j, Board board) {
        //  * <-
        //   **
        //  *
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 2) == gemStartType &&
                    board.getGemAtPosition(i + 2, j) == gemStartType && board.getGemAtPosition(i + 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase4(int i,int j, Board board) {
        //  *
        //** **
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 2) == gemStartType &&
                    board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase5(int i,int j, Board board) {
        //** **
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j + 2) == gemStartType &&
                    board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i - 1, j - 2) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase6(int i,int j, Board board) {
        //** *
        //  *
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i, j - 2) == gemStartType && board.getGemAtPosition(i, j - 3) == gemStartType &&
                    board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 2, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase7(int i,int j, Board board) {
        //  *
        //**
        //  *
        //  *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 1, j - 2) == gemStartType &&
                    board.getGemAtPosition(i + 2, j) == gemStartType && board.getGemAtPosition(i + 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase8(int i,int j, Board board) {
        // *
        //* **
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j + 1) == gemStartType &&
                    board.getGemAtPosition(i, j + 2) == gemStartType && board.getGemAtPosition(i, j + 3) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase9(int i,int j, Board board) {
        // *
        // *
        //  **
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j + 2) == gemStartType &&
                    board.getGemAtPosition(i - 2, j) == gemStartType && board.getGemAtPosition(i - 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase10(int i,int j, Board board) {
        // *
        // *
        //*  **
        //
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i, j + 2) == gemStartType && board.getGemAtPosition(i, j + 2) == gemStartType &&
                    board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 2, j + 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase11(int i,int j, Board board) {
        // *
        // *
        //* *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i - 2, j) == gemStartType && board.getGemAtPosition(i - 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase12(int i,int j, Board board) {
        // *
        //* *
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 1, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 2, j) == gemStartType && board.getGemAtPosition(i + 3, j) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase13(int i,int j, Board board) {
        // *
        // *
        //  *
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j - 1) == gemStartType && board.getGemAtPosition(i - 2, j - 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j - 1) == gemStartType && board.getGemAtPosition(i + 2, j - 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }

    public boolean checkForFiveMatchCase14(int i,int j, Board board) {
        // *
        // *
        //*
        // *
        // *
        int gemStartType = board.getGemAtPosition(i, j);
        if(gemStartType != 0) {

            if (board.getGemAtPosition(i - 1, j + 1) == gemStartType && board.getGemAtPosition(i - 2, j + 1) == gemStartType &&
                    board.getGemAtPosition(i + 1, j + 1) == gemStartType && board.getGemAtPosition(i + 2, j + 1) == gemStartType) {
                return true;
            }
        }
        return false;
    }



}
